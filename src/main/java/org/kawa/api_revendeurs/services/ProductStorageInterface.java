package org.kawa.api_revendeurs.services;

import org.kawa.api_revendeurs.domain.Product;

import java.util.List;

public interface ProductStorageInterface {
    List<Product> getProducts();

    Product getProduct(String id);
}
