package org.kawa.api_revendeurs.services;

import java.util.Optional;
import java.util.UUID;

public interface AuthenticationInterface {

    void storeAuthKeyForResellerName(String username, UUID authKey);

    Optional<String> getHashedAuthKeyForResellerName(String username);
}
