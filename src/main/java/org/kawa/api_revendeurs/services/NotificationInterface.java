package org.kawa.api_revendeurs.services;

import org.kawa.api_revendeurs.domain.Reseller;

public interface NotificationInterface {
    void notifyCreation(Reseller reseller);
}
