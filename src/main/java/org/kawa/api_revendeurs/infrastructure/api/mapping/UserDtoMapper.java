package org.kawa.api_revendeurs.infrastructure.api.mapping;

import org.kawa.api_revendeurs.domain.Reseller;
import org.kawa.api_revendeurs.infrastructure.MapstructSettings;
import org.kawa.api_revendeurs.infrastructure.api.serialization.UserDto;
import org.mapstruct.Mapper;

@Mapper(config = MapstructSettings.class)
public interface UserDtoMapper {
    Reseller from(UserDto dto);
}
