package org.kawa.api_revendeurs.infrastructure.api;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import org.kawa.api_revendeurs.domain.Product;
import org.kawa.api_revendeurs.infrastructure.api.mapping.ProductApiDtoMapper;
import org.kawa.api_revendeurs.infrastructure.api.serialization.ProductApiDto;
import org.kawa.api_revendeurs.services.ProductService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/products", consumes = {"*/*"}, produces = {"application/json"})
@Validated
public class ProductController {
    private final ProductService productService;
    private final ProductApiDtoMapper productApiDtoMapper;

    @GetMapping
    public List<ProductApiDto> getProducts() {
        List<Product> products = productService.getProducts();
        return productApiDtoMapper.toProductDtoList(products);
    }

    @GetMapping("{id}")
    public ProductApiDto getProduct(@PathVariable @NotBlank @Valid String id) {
        Product product = productService.getProduct(id);
        return productApiDtoMapper.toProductDto(product);
    }

    public ProductController(ProductService productService, ProductApiDtoMapper productApiDtoMapper) {
        this.productService = productService;
        this.productApiDtoMapper = productApiDtoMapper;
    }
}
