package org.kawa.api_revendeurs.infrastructure.api;

import jakarta.validation.constraints.NotNull;
import org.kawa.api_revendeurs.infrastructure.api.mapping.UserDtoMapper;
import org.kawa.api_revendeurs.infrastructure.api.serialization.UserDto;
import org.kawa.api_revendeurs.infrastructure.notifications.MailQrCodeNotification;
import org.kawa.api_revendeurs.services.ResellerService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api")
@RestController
@Validated
public class ResellerController {
    private final ResellerService resellerService;
    private final UserDtoMapper mapper;
    private final MailQrCodeNotification defaultNotificationMethod;

    public ResellerController(ResellerService resellerService, UserDtoMapper mapper, MailQrCodeNotification defaultNotificationMethod) {
        this.resellerService = resellerService;
        this.mapper = mapper;
        this.defaultNotificationMethod = defaultNotificationMethod;
    }

    @PostMapping("inscription")
    public ResponseEntity<Void> createAccount(@NotNull @RequestBody UserDto user) {
        resellerService.registerReseller(mapper.from(user), defaultNotificationMethod);
        return ResponseEntity.status(201).build();
    }
}
