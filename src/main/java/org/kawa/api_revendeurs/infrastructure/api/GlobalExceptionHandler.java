package org.kawa.api_revendeurs.infrastructure.api;

import org.example.domaine.NotFoundException;
import org.example.domaine.ServerException;
import org.kawa.api_revendeurs.domain.InvalidEmailException;
import org.kawa.api_revendeurs.domain.InvalidUsernameException;
import org.kawa.api_revendeurs.infrastructure.notifications.MailSendingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.RestClientException;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    private static final String HTTP4XX_MSG = "HTTP 4xx error";
    private static final String HTTP5XX_MSG = "HTTP 5xx error";

    @ExceptionHandler({InvalidEmailException.class, InvalidUsernameException.class})
    ProblemDetail onInvalidInputData(RuntimeException invalidException) {
        log(Level.DEBUG, HTTP4XX_MSG, invalidException);
        return ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, invalidException.getMessage());
    }

    @ExceptionHandler(MailSendingException.class)
    ProblemDetail onTechnicalError(RuntimeException technicalException) {
        log(Level.ERROR, HTTP5XX_MSG, technicalException);
        return ProblemDetail.forStatusAndDetail(HttpStatus.INTERNAL_SERVER_ERROR, technicalException.getMessage());
    }

    @ExceptionHandler(RestClientException.class)
    ProblemDetail onErpError(RestClientException restClientException) {
        log(Level.ERROR, HTTP5XX_MSG, restClientException);
        return ProblemDetail.forStatusAndDetail(HttpStatus.BAD_GATEWAY, "Erreur d'accès aux données");
    }

    @ExceptionHandler(NotFoundException.class)
    ProblemDetail onLocalServerError(NotFoundException notFoundException) {
        log(Level.ERROR, HTTP4XX_MSG, notFoundException);
        return ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, notFoundException.getMessage());
    }

    @ExceptionHandler(ServerException.class)
    ProblemDetail onLocalServerError(ServerException serverException) {
        log(Level.ERROR, HTTP5XX_MSG, serverException);
        return ProblemDetail.forStatusAndDetail(HttpStatus.BAD_GATEWAY, serverException.getMessage());
    }

    private void log(Level level, String message, Exception cause) {
        if (LOG.isEnabledForLevel(level)) {
            var logEntry = LOG.atLevel(level).setMessage(message);
            if (cause != null) {
                logEntry = logEntry.setCause(cause);
            }
            logEntry.log();
        }
    }
}
