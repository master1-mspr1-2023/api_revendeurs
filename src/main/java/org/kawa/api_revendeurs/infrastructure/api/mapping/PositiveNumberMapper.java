package org.kawa.api_revendeurs.infrastructure.api.mapping;

import org.example.domaine.PositiveNumber;
import org.springframework.stereotype.Component;

@Component
public class PositiveNumberMapper {
    public <T extends Number> T unwrap(PositiveNumber<T> value) {
        return value.getNumber();
    }
    public <T extends Number> PositiveNumber<T> wrap(T value) {
        return new PositiveNumber<>(value);
    }
}
