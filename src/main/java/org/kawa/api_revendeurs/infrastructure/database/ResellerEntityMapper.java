package org.kawa.api_revendeurs.infrastructure.database;

import org.kawa.api_revendeurs.domain.Reseller;
import org.kawa.api_revendeurs.infrastructure.MapstructSettings;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructSettings.class)
public interface ResellerEntityMapper {
    @Mapping(target = "authKey", ignore = true)
    ResellerEntity from(Reseller reseller);
}
