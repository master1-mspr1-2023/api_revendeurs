package org.kawa.api_revendeurs.infrastructure.database;

import org.kawa.api_revendeurs.domain.Reseller;
import org.kawa.api_revendeurs.services.ResellerStorageInterface;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Component
public class ResellerDBStorage implements ResellerStorageInterface {
    private final ResellerRepository resellerRepository;
    private final ResellerEntityMapper resellerEntityMapper;

    public ResellerDBStorage(ResellerRepository resellerRepository, ResellerEntityMapper resellerEntityMapper) {
        this.resellerRepository = resellerRepository;
        this.resellerEntityMapper = resellerEntityMapper;
    }

    @Override
    @Transactional
    public void runAtomically(Runnable atomicAction) {
        atomicAction.run();
    }

    @Override
    @Transactional
    public void saveReseller(Reseller reseller) {
        var resellerEntity = resellerEntityMapper.from(reseller);
        resellerRepository.save(resellerEntity);
    }
}
