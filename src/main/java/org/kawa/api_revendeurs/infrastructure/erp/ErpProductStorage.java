package org.kawa.api_revendeurs.infrastructure.erp;


import org.example.domaine.NotFoundException;
import org.example.domaine.ServerException;
import org.example.infrastructure.ErpProductDTO;
import org.kawa.api_revendeurs.domain.Product;
import org.kawa.api_revendeurs.services.ProductStorageInterface;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Component
public class ErpProductStorage implements ProductStorageInterface {
    private final ApiProperties properties;
    private final RestTemplate restTemplate;
    private final ErpProductMapper productMapper;

    public ErpProductStorage(ApiProperties properties, RestTemplate restTemplate, ErpProductMapper productMapper) {
        this.properties = properties;
        this.restTemplate = restTemplate;
        this.productMapper = productMapper;
    }

    public List<Product> getProducts() {
        var response = restTemplate.exchange(
            properties.productsUrl(),
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<ErpProductDTO>>() {
            }
        );
        List<ErpProductDTO> productsDTOs = Optional.ofNullable(response).map(ResponseEntity::getBody)
            .orElseThrow(() -> new ServerException("Erreur de récupération des produits"));
        return productMapper.fromErpProductDtos(productsDTOs);
    }

    @Override
    public Product getProduct(String id) {
        var response = restTemplate.exchange(
            properties.productsUrl() + "/" + id,
            HttpMethod.GET,
            null,
            ErpProductDTO.class
        );
        if (response.getStatusCode().value() == 404) {
            throw new NotFoundException("Le produit demandé n'a pas été trouvé");
        } else if (response.getStatusCode().is4xxClientError()) {
            throw new NotFoundException("L'ID de produit fourni n'est pas valide");
        } else if (response.getStatusCode().is5xxServerError() || response.getBody() == null) {
            throw new ServerException("Erreur de récupération du produit");
        }
        return productMapper.fromErpProductDto(response.getBody());
    }
}
