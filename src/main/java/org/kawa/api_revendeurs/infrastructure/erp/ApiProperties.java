package org.kawa.api_revendeurs.infrastructure.erp;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "erp.api")
public record ApiProperties(String baseUrl, String productsUrl, String customersUrl) {
}
