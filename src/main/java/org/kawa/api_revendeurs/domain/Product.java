package org.kawa.api_revendeurs.domain;

import org.example.domaine.PositiveNumber;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

public class Product {
    private final String id;
    private final Instant createdAt;
    private final String name;
    private final PositiveNumber<BigDecimal> price;
    private final String description;
    private final String color;
    private final PositiveNumber<Integer> stock;

    public Product(String id, Instant createdAt, String name, PositiveNumber<BigDecimal> price, String description, String color, PositiveNumber<Integer> stock) {
        this.id = Objects.requireNonNull(id).trim();
        this.createdAt = Objects.requireNonNull(createdAt);
        this.name = Objects.requireNonNull(name).trim();
        this.price = Objects.requireNonNull(price);
        this.description = Objects.requireNonNull(description).trim();
        this.color = Objects.requireNonNull(color).trim();
        this.stock = Objects.requireNonNull(stock);
        if (this.id.isEmpty() || this.name.isEmpty() || this.description.isEmpty() || this.color.isBlank()) {
            throw new IllegalArgumentException();
        }
    }

    public PositiveNumber<Integer> getStock() {
        return stock;
    }


    public String getId() {
        return id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public String getName() {
        return name;
    }

    public PositiveNumber<BigDecimal> getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + id +
            ", createdAt=" + createdAt +
            ", name='" + name + '\'' +
            ", price=" + price +
            ", description='" + description + '\'' +
            ", color='" + color + '\'' +
            ", stock=" + stock +
            '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Product product)) {
            return false;
        } else {
            return List.of(this.id, this.createdAt, this.name, this.price, this.description, this.color, this.stock).equals(
                List.of(product.id, product.createdAt, product.name, product.price, product.description, product.color, product.stock));
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.createdAt, this.name, this.price, this.description, this.color, this.stock);
    }
}
