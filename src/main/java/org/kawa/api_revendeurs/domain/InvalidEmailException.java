package org.kawa.api_revendeurs.domain;

public class InvalidEmailException extends RuntimeException {
    public InvalidEmailException() {
        super("L'e-mail fourni doit-être un e-mail valide");
    }
}
