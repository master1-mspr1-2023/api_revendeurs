package org.kawa.api_revendeurs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.domaine.DateTimeMapper;
import org.example.domaine.NumberMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@ConfigurationPropertiesScan
public class ApiRevendeursApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiRevendeursApplication.class, args);
    }

    @Bean
    ObjectMapper objectMapper() {
        var result = new ObjectMapper();
        result.registerModule(new JavaTimeModule());
        result.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return result;
    }

    @Bean
    DateTimeMapper dateTimeMapper() {
        return new DateTimeMapper();
    }

    @Bean
    NumberMapper numberMapper() {
        return new NumberMapper();
    }
}
