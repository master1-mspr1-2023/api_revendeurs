package org.kawa.api_revendeurs.domain;

import org.assertj.core.api.Assertions;
import org.example.domaine.PositiveNumber;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.function.Supplier;
import java.util.stream.Stream;

class ProductTest {
    private static final Instant NOW = Instant.now();
    private static final PositiveNumber<BigDecimal> BIG_TEN = new PositiveNumber<>(BigDecimal.TEN);
    private static final PositiveNumber<Integer> INT_ONE = new PositiveNumber<>(1);

    @ParameterizedTest
    @MethodSource("productConstructorsWithNullParameter")
    void allProductFieldsAreMandatory(Supplier<Product> productWithNullParam) {
        Assertions.assertThatThrownBy(() -> productWithNullParam.get()).isInstanceOf(NullPointerException.class);
    }

    static Stream<Arguments> productConstructorsWithNullParameter() {
        return Stream.of(
            Arguments.of((Supplier<Product>) () -> new Product(null, NOW, "n", BIG_TEN, "d", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", null, "n", BIG_TEN, "d", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, null, BIG_TEN, "d", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, "n", null, "d", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, "n", BIG_TEN, null, "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, "n", BIG_TEN, "d", null, INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, "n", BIG_TEN, "d", "c", null))
        );
    }

    @Test
    void allStringArgumentsAreTrimmed() {
        Assertions
            .assertThat(new Product(" 3 ", NOW, " n ", BIG_TEN, " d ", " c ", INT_ONE))
            .isEqualTo(new Product("3", NOW, "n", BIG_TEN, "d", "c", INT_ONE));
    }

    @ParameterizedTest
    @MethodSource("productConstructorsWithBlankParameter")
    void blankStringArgumentsAreForbiden(Supplier<Product> productWithBlankParam) {
        Assertions.assertThatThrownBy(() -> productWithBlankParam.get()).isInstanceOf(IllegalArgumentException.class);
    }

    static Stream<Arguments> productConstructorsWithBlankParameter() {
        return Stream.of(
            Arguments.of((Supplier<Product>) () -> new Product(" ", NOW, "n", BIG_TEN, "d", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, " ", BIG_TEN, "d", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, "n", BIG_TEN, " ", "c", INT_ONE)),
            Arguments.of((Supplier<Product>) () -> new Product("3", NOW, "n", BIG_TEN, "d", " ", INT_ONE))
        );
    }
}
