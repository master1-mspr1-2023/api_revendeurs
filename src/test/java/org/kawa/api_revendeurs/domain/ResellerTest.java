package org.kawa.api_revendeurs.domain;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

class ResellerTest {

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = "  ")
    void usernameIsNeitherNullNorEmpty(String invalidUsername) {
        Assertions.assertThatThrownBy(() -> new Reseller(invalidUsername, "test@example.org"))
            .isInstanceOf(InvalidUsernameException.class);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"  ", "no-domain", "@no.user", "no@top-level-domain", "forbiden!@:-).com"})
    void emailIsNeitherNullNorEmptyAndMustBeAnEmail(String invalidEmail) {
        Assertions.assertThatThrownBy(() -> new Reseller("username", invalidEmail))
            .isInstanceOf(InvalidEmailException.class);
    }

    @Test
    void nonBlankTrimmedUsernameAndValidTrimmedEmailAreOK() {
        Assertions
            .assertThat(new Reseller(" user ", " user@d.org "))
            .isEqualTo(new Reseller("user", "user@d.org"));
    }
}
