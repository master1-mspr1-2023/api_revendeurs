package org.kawa.api_revendeurs.spring;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.util.TestSocketUtils;

// https://faboo.org/2018/10/dynamic-port-spring-test/
// https://www.baeldung.com/junit-5-extensions
// https://junit.org/junit5/docs/5.1.1/api/org/junit/jupiter/api/extension/RegisterExtension.html
@Component
public class GreenMailJUnitExtension implements BeforeEachCallback, AfterEachCallback {

    @Value("${spring.mail.port}")
    private int smtpPort;

    @Value("${spring.mail.username}")
    private String smtpUser;

    @Value("${spring.mail.password}")
    private String smtpPass;

    private GreenMail smtpServer;

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        smtpServer = new GreenMail(new ServerSetup(smtpPort, null, ServerSetup.PROTOCOL_SMTP));
        smtpServer.setUser(smtpUser, smtpPass);
        smtpServer.start();
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) throws Exception {
        smtpServer.stop();
    }

    public MimeMessage[] getReceivedMessages() {
        return smtpServer.getReceivedMessages();
    }

    public static class GreenMailPortInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            int randomPort = TestSocketUtils.findAvailableTcpPort();
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(applicationContext,
                "spring.mail.port=" + randomPort);
        }
    }
}
